using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MusicLibrarySuite.AspNetCore.Hosting;

/// <summary>
/// Represents an abstract implementation of the <see cref="IStartupEx" /> interface.
/// <para>
/// The <see cref="ConfigureServices(IServiceCollection)" /> method
/// must implemented by the <c>Startup</c> class of the application.
/// </para>
/// </summary>
public abstract class StartupBase : IStartupEx
{
    /// <inheritdoc />
    public IConfiguration Configuration { get; }

    /// <inheritdoc />
    public IHostEnvironment HostEnvironment { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="StartupBase" /> type using the application configuration.
    /// </summary>
    /// <param name="configuration">The application configuration.</param>
    /// <param name="hostEnvironment">The hosting environment information provider.</param>
    protected StartupBase(IConfiguration configuration, IHostEnvironment hostEnvironment)
    {
        Configuration = configuration;
        HostEnvironment = hostEnvironment;
    }

    /// <inheritdoc />
    public abstract void ConfigureServices(IServiceCollection services);
}
