using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MusicLibrarySuite.AspNetCore.Hosting;

/// <summary>
/// Represents an abstract implementation of the <see cref="IWebStartupEx" /> interface.
/// <para>
/// The <see cref="StartupBase.ConfigureServices(IServiceCollection)" /> and <see cref="Configure(IApplicationBuilder)" /> methods
/// must implemented by the <c>WebStartup</c> class of the application.
/// </para>
/// </summary>
public abstract class WebStartupBase : StartupBase, IWebStartupEx
{
    /// <inheritdoc />
    public IWebHostEnvironment WebHostEnvironment { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="WebStartupBase" /> type using the application configuration and web hosting environment information provider.
    /// </summary>
    /// <param name="configuration">The application configuration.</param>
    /// <param name="webHostEnvironment">The web hosting environment information provider.</param>
    protected WebStartupBase(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        : base(configuration, webHostEnvironment)
    {
        WebHostEnvironment = webHostEnvironment;
    }

    /// <inheritdoc />
    public abstract void Configure(IApplicationBuilder applicationBuilder);
}
