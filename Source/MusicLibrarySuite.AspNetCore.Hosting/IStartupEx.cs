using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MusicLibrarySuite.AspNetCore.Hosting;

/// <summary>
/// Describes a <c>Startup</c> class for an application using the .NET generic host.
/// </summary>
public interface IStartupEx
{
    /// <summary>
    /// Gets the application's set of key/value configuration properties..
    /// </summary>
    public IConfiguration Configuration { get; }

    /// <summary>
    /// Gets the application's hosting environment information.
    /// </summary>
    public IHostEnvironment HostEnvironment { get; }

    /// <summary>
    /// Adds services to the container.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection" /> collection for adding service descriptors.</param>
    /// <remarks>This method gets called by the runtime. Use this method to add services to the container.</remarks>
    public void ConfigureServices(IServiceCollection services);
}
